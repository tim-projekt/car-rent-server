package com.cloud.car.server.repository;

import com.cloud.car.server.model.Reservation;
import com.cloud.car.server.model.enums.ReservationStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ReservationRepository extends JpaRepository<Reservation, Long> {
	@Query(value="SELECT * FROM reservation r WHERE r.status = 'ACTIVE'" , nativeQuery=true)
	Iterable<Reservation> getActiveReservations();
	
	@Query(value="SELECT * FROM reservation r WHERE r.status = 'ACTIVE' AND r.car_id = :car_id" , nativeQuery=true)
	Reservation getReservationByCar(@Param(value = "car_id") Long car_id);
	
	@Query(value="SELECT * FROM reservation r WHERE r.status = 'ACTIVE' AND r.user_id = :userId" , nativeQuery=true)
	Reservation getActiveReservationByUser(@Param(value = "userId") Long userId);

	Reservation getAllByStatus(ReservationStatus reservationStatus);
	Reservation findReservationById(Long id);
}
