package com.cloud.car.server.repository;

import com.cloud.car.server.model.TariffPlan;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface TariffRepository extends CrudRepository<TariffPlan, Long>{
	@Query(value="SELECT * FROM tariff_plan t WHERE t.status = 'ACTIVE' ORDER BY minutes ASC" , nativeQuery=true)
	Iterable<TariffPlan> getSortedTariffs();
}
