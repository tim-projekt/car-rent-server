package com.cloud.car.server.repository;

import com.cloud.car.server.model.City;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CityRepository extends JpaRepository<City, Long> {
    City findCityById(Long id);
}
