package com.cloud.car.server.repository;

import com.cloud.car.server.model.Renting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface RentingRepository extends JpaRepository<Renting, Long> {
	@Query(value="SELECT * FROM renting r WHERE r.end_time IS NULL AND r.user_id = :userId" , nativeQuery=true)
	Renting getActiveRentingByUser(@Param(value = "userId") Long userId);
	
	@Query(value="SELECT * FROM renting r WHERE r.user_id = :userId  ORDER BY r.start_time DESC OFFSET :offset LIMIT :limit" , nativeQuery=true)
	Iterable<Renting> getMyLastHistory(@Param(value = "userId") Long userId,
									   @Param(value = "offset") Integer offset,	 
									   @Param(value = "limit") Integer limit);
	
	@Query(value="SELECT count(*) FROM renting r WHERE r.user_id = :userId" , nativeQuery=true)
	Integer getTotalNumberOfRentingsByUserId(@Param(value = "userId") Long userId);

	Renting findRentingById(Long id);
}
