package com.cloud.car.server.repository;

import com.cloud.car.server.model.Parking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ParkingRepository  extends JpaRepository<Parking, Long> {

  @Query(value="SELECT * FROM parking p WHERE p.lat_lngx= :lngx ADN p.lat_lngy= :lngy" , nativeQuery=true)
	Parking getParkingByXY(@Param("lngx") Double lngx,
				   		   @Param("lngx") Double lngy);

  Parking findParkingById(Long id);
  Parking findParkingByLatLngXAndLatLngY(Double lngx, Double lngy);
}
