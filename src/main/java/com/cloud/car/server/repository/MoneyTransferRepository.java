package com.cloud.car.server.repository;

import com.cloud.car.server.model.MoneyTransfer;
import org.springframework.data.repository.CrudRepository;

public interface MoneyTransferRepository extends CrudRepository<MoneyTransfer, Long>{

}
