package com.cloud.car.server.repository;

import com.cloud.car.server.model.Region;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RegionRepository extends JpaRepository<Region, Long> {
    Region findRegionById(Long id);
}
