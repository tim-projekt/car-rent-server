package com.cloud.car.server.repository;

import com.cloud.car.server.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByPhone(String phone);
}
