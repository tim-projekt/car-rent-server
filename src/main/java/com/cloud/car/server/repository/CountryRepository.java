package com.cloud.car.server.repository;

import com.cloud.car.server.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepository extends JpaRepository<Country, Long> {
    Country findCountryById(Long id);
}
