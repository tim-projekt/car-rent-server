package com.cloud.car.server.repository;

import com.cloud.car.server.model.Car;
import com.cloud.car.server.model.enums.CarStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface CarRepository extends JpaRepository<Car, Long> {
	@Query(value="SELECT * FROM car c WHERE c.status = 'ACTIVE'" , nativeQuery=true)
	Iterable<Car> getActiveCars();

	List<Car> findAllByStatus(CarStatus carStatus);
	Car findCarById(Long id);
}
