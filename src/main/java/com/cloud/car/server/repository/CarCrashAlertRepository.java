package com.cloud.car.server.repository;

import com.cloud.car.server.model.CarCrashAlerts;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface CarCrashAlertRepository extends JpaRepository<CarCrashAlerts, Long> {

}
