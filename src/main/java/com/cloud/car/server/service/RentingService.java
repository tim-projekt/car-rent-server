package com.cloud.car.server.service;

import com.cloud.car.server.exception.AccountBalanceUnderLimitException;
import com.cloud.car.server.model.Reservation;
import com.cloud.car.server.model.TariffPlan;
import com.cloud.car.server.model.dto.HistoryDTO;
import com.cloud.car.server.model.dto.RentingDTO;
import com.cloud.car.server.model.dto.TariffPlanDTO;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface RentingService {
    RentingDTO getUserRenting();
    RentingDTO getRenting(Long id);
    RentingDTO startRenting(RentingDTO rentingDTO) throws AccountBalanceUnderLimitException;
    RentingDTO endRenting(Long id, RentingDTO rentingDTO);
    boolean updateReservation(Reservation reservation);
    List<TariffPlanDTO> getTariffs();
    HistoryDTO getMyLastHistory(Integer page, Integer pageSize);
}
