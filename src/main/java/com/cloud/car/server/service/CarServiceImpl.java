package com.cloud.car.server.service;

import com.cloud.car.server.model.Car;
import com.cloud.car.server.model.CarCrashAlerts;
import com.cloud.car.server.model.User;
import com.cloud.car.server.model.dto.carCrashAlertDTO.CarCrashAlertDTO;
import com.cloud.car.server.model.dto.carCrashAlertDTO.CarCrashAlertResponseDTO;
import com.cloud.car.server.model.dto.carDTO.CarCreateDTO;
import com.cloud.car.server.model.dto.carDTO.CarDTO;
import com.cloud.car.server.model.enums.CarStatus;
import com.cloud.car.server.repository.CarCrashAlertRepository;
import com.cloud.car.server.repository.CarRepository;
import com.cloud.car.server.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;

@Service
public class CarServiceImpl implements CarService {

    private final CarRepository carRepository;
    private final UserRepository userRepository;
    private final CarCrashAlertRepository crashRepository;
    private final ModelMapper mapper;

    private Type listType = new TypeToken<List<CarDTO>>() {}.getType();

    @Autowired
    public CarServiceImpl(CarRepository carRepository,
                          UserRepository userRepository,
                          CarCrashAlertRepository crashRepository,
                          ModelMapper mapper) {
        this.carRepository = carRepository;
        this.userRepository = userRepository;
        this.crashRepository = crashRepository;
        this.mapper = mapper;
    }

    @Override
    public CarDTO createCar(CarCreateDTO carDTO) {
        Car car = mapper.map(carDTO, Car.class);
        if (car.getPurchaseDate() == null)
            car.setPurchaseDate(new Date());
        return mapper.map(carRepository.save(car), CarDTO.class);
    }

    @Override
    public List<CarDTO> getActiveCars() {
        return mapper.map(carRepository.findAllByStatus(CarStatus.ACTIVE), listType);
    }

    @Override
    public List<CarDTO> getAllCars() {
        return mapper.map(carRepository.findAll(), listType);
    }

    @Override
    public CarDTO getCar(Long id) {
        return mapper.map(carRepository.findCarById(id), CarDTO.class);
    }

    @Override
    public CarDTO updateCar(Long id, CarDTO carDTO) {
        Car car = carRepository.findCarById(id);
        if (car != null) {
            if (!carDTO.equals(mapper.map(car, CarDTO.class))) {
                Car car1 = mapper.map(carDTO, Car.class);
                car1.setId(car.getId());
                if(carDTO.getPurchaseDate() == null)
                    car1.setPurchaseDate(car.getPurchaseDate());
                return mapper.map(carRepository.save(car1), CarDTO.class);
            }
        }
        return null;
    }

    @Override
    public void deleteCar(Long id) {
        carRepository.deleteById(id);
    }

    @Override
    public CarCrashAlertResponseDTO addCrashAlert(Long id, CarCrashAlertDTO alertDTO) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String phone = auth.getName(); // get logged user
        User user = userRepository.findByPhone(phone);
        CarCrashAlerts alert = mapper.map(alertDTO, CarCrashAlerts.class);
        alert.setUser(user);
        Car car = carRepository.findCarById(id);
        if (car != null) {
            car.setStatus(CarStatus.BREAK);
            car = carRepository.save(car);
            alert.setCar(car);
            alert.getCar().setStatus(CarStatus.BREAK);
            alert = crashRepository.save(alert);
            CarCrashAlertResponseDTO dto = mapper.map(alert, CarCrashAlertResponseDTO.class);
            dto.setCar(mapper.map(alert.getCar(), CarDTO.class));
            return dto;
        }
        return null;
    }
}
