package com.cloud.car.server.service;

import com.cloud.car.server.model.Car;
import com.cloud.car.server.model.Reservation;
import com.cloud.car.server.model.User;
import com.cloud.car.server.model.dto.ReservationDTO;
import com.cloud.car.server.model.dto.carDTO.CarDTO;
import com.cloud.car.server.model.enums.CarStatus;
import com.cloud.car.server.model.enums.ReservationStatus;
import com.cloud.car.server.repository.CarRepository;
import com.cloud.car.server.repository.ReservationRepository;
import com.cloud.car.server.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class ReservationServiceImpl implements ReservationService {

    private final ReservationRepository reservationRepository;
    private final UserRepository userRepository;
    private final CarRepository carRepository;
    private final ModelMapper mapper;

    @Autowired
    public ReservationServiceImpl(ReservationRepository reservationRepository,
                                  UserRepository userRepository,
                                  CarRepository carRepository,
                                  ModelMapper mapper) {
        this.reservationRepository = reservationRepository;
        this.userRepository = userRepository;
        this.carRepository = carRepository;
        this.mapper = mapper;
    }

    @Override
    public ReservationDTO createReservation(ReservationDTO reservationDTO) {
        String phone = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userRepository.findByPhone(phone);
        Reservation reservation = mapper.map(reservationDTO, Reservation.class);

        reservation.setStatus(ReservationStatus.ACTIVE);
        reservation.setEndTime(null);
        reservation.setStartTime(new Date().getTime());
        reservation.setUser(user);
        Optional<Car> carOpt = carRepository.findById(reservationDTO.getCar().getId());
        if (carOpt.isPresent()) {
            if (carOpt.get().getStatus().equals(CarStatus.ACTIVE)) {
                Car car = carOpt.get();
                car.setStatus(CarStatus.RESERVED);
                reservation.setCar(car);
                carRepository.save(car);
                Reservation reservationDB = reservationRepository.save(reservation);
                ReservationDTO dto = mapper.map(reservationDB, ReservationDTO.class);
                dto.setCar(mapper.map(reservationDB.getCar(), CarDTO.class));
                return dto;
            }
        }

        return null;
    }

    @Override
    public ReservationDTO getReservations() {
        String phone = SecurityContextHolder.getContext().getAuthentication().getName();
        Long userId = userRepository.findByPhone(phone).getId();
        Reservation reservation = reservationRepository.getActiveReservationByUser(userId);
        if (reservation != null) {
            ReservationDTO reservationDTO = mapper.map(reservation, ReservationDTO.class);
            reservationDTO.setCar(mapper.map(reservation.getCar(), CarDTO.class));
            return reservationDTO;
        }
        return null;

    }

    @Override
    public ReservationDTO getReservation(Long id) {
        Reservation reservationDB = reservationRepository.findReservationById(id);
        ReservationDTO reservationDTO = mapper.map(reservationDB, ReservationDTO.class);
        reservationDTO.setCar(mapper.map(reservationDB.getCar(), CarDTO.class));
        return reservationDTO;
    }

    @Override
    public boolean deleteReservation(Long id) {
        Optional<Reservation> reservationOpt = reservationRepository.findById(id);
        if (reservationOpt.isPresent()) {
            Optional<Car> carOpt = carRepository.findById(reservationOpt.get().getCar().getId());
            if (carOpt.isPresent()) {
                Car car = carOpt.get();
                car.setStatus(CarStatus.ACTIVE);
                carRepository.save(car);
            }
            reservationRepository.deleteById(id);
            return true;
        }
        return false;
    }
}
