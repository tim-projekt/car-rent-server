package com.cloud.car.server.service;

import com.cloud.car.server.model.dto.carCrashAlertDTO.CarCrashAlertDTO;
import com.cloud.car.server.model.dto.carCrashAlertDTO.CarCrashAlertResponseDTO;
import com.cloud.car.server.model.dto.carDTO.CarCreateDTO;
import com.cloud.car.server.model.dto.carDTO.CarDTO;

import java.util.List;

public interface CarService {
    CarDTO createCar(CarCreateDTO car);
    List<CarDTO> getActiveCars();
    List<CarDTO> getAllCars();
    CarDTO getCar(Long id);
    CarDTO updateCar(Long id, CarDTO car);
    void deleteCar(Long id);
    CarCrashAlertResponseDTO addCrashAlert(Long id, CarCrashAlertDTO carCrashAlertDTO);
}
