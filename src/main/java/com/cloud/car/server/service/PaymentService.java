package com.cloud.car.server.service;

import com.cloud.car.server.exception.PaymentException;
import com.cloud.car.server.model.MoneyTransfer;
import com.cloud.car.server.model.enums.PaymentStatus;
import com.cloud.car.server.repository.MoneyTransferRepository;
import com.cloud.car.server.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.logging.Logger;

@Service
public class PaymentService {

	private Logger log = Logger.getLogger(this.getClass().getName());

	private final MoneyTransferRepository moneyTransferRepository;

	private final UserRepository userRepository;

	@Autowired
	public PaymentService(MoneyTransferRepository moneyTransferRepository, UserRepository userRepository) {
		this.moneyTransferRepository = moneyTransferRepository;
		this.userRepository = userRepository;
	}


	public ResponseEntity<?> initPayment(MoneyTransfer moneyTransfer) throws PaymentException {
		try {
			// TODO implement real payments
			moneyTransfer.setStatus(PaymentStatus.COMPLETED);
			
			switch (moneyTransfer.getStatus()) {
			case PENDING:
				log.info("PENDING");
				moneyTransfer.setStatus(PaymentStatus.PENDING);
				break;
			case COMPLETED:
				log.info("COMPLETED");
				moneyTransfer.setStatus(PaymentStatus.COMPLETED);
				Double newAccBalance = moneyTransfer.getUser().getAccountBalance() + moneyTransfer.getAmount();
				moneyTransfer.getUser().setAccountBalance(newAccBalance);
				userRepository.save(moneyTransfer.getUser());
				break;
			case CANCELED:
				log.info("CANCELED");
				moneyTransfer.setStatus(PaymentStatus.CANCELED);
				break;
			default:
				log.info("404 " + moneyTransfer.getStatus());
				break;
			}
			moneyTransferRepository.save(moneyTransfer);

			log.info("*** PAYMENT STATUS ***");
			log.info(moneyTransfer.toString());
			return new ResponseEntity<>(moneyTransfer, HttpStatus.OK);
		} catch (Exception e) {
			throw new PaymentException(e);
		}
	}
}
