package com.cloud.car.server.service;

import com.cloud.car.server.constant.Constants;
import com.cloud.car.server.exception.AccountBalanceUnderLimitException;
import com.cloud.car.server.model.*;
import com.cloud.car.server.model.dto.HistoryDTO;
import com.cloud.car.server.model.dto.RentingDTO;
import com.cloud.car.server.model.dto.TariffPlanDTO;
import com.cloud.car.server.model.dto.carDTO.CarDTO;
import com.cloud.car.server.model.enums.CarStatus;
import com.cloud.car.server.model.enums.PaymentStatus;
import com.cloud.car.server.model.enums.ReservationStatus;
import com.cloud.car.server.repository.*;
import com.google.common.collect.Lists;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class RentingServiceImpl implements RentingService {

    private final UserRepository userRepository;
    private final RentingRepository rentingRepository;
    private final ReservationRepository reservationRepository;
    private final CarRepository carRepository;
    private final TariffRepository tariffRepository;
    private final MoneyTransferRepository moneyTransferRepository;
    private final ModelMapper mapper;

    @Autowired
    public RentingServiceImpl(UserRepository userRepository,
                              RentingRepository rentingRepository,
                              ReservationRepository reservationRepository,
                              CarRepository carRepository,
                              TariffRepository tariffRepository,
                              MoneyTransferRepository moneyTransferRepository,
                              ModelMapper mapper) {
        this.userRepository = userRepository;
        this.rentingRepository = rentingRepository;
        this.reservationRepository = reservationRepository;
        this.carRepository = carRepository;
        this.tariffRepository = tariffRepository;
        this.moneyTransferRepository = moneyTransferRepository;
        this.mapper = mapper;
    }

    @Override
    public RentingDTO getUserRenting() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userRepository.findByPhone(auth.getName());
        Renting renting = rentingRepository.getActiveRentingByUser(user.getId());
        return getRentingDTO(renting);
    }

    @Override
    public RentingDTO getRenting(Long id) {
        Renting renting = rentingRepository.findRentingById(id);
        return getRentingDTO(renting);
    }

    @Override
    public RentingDTO startRenting(RentingDTO rentingDTO) throws AccountBalanceUnderLimitException {
        User user = getLoggedUser();
        if (user.getAccountBalance() < 3.0) {
            throw new AccountBalanceUnderLimitException("You have less then 3PLN");
        }
        Optional<Car> carOpt = carRepository.findById(rentingDTO.getCar().getId());
        Car car;
        if (carOpt.isPresent()) {
            car = carOpt.get();
            if (car.getStatus() == CarStatus.RESERVED) {
                // car reserved
                Reservation reservation = reservationRepository.getReservationByCar(car.getId());
                if (reservation.getUser().getId().equals(user.getId())) {
                    Reservation reservationDTO = new Reservation();
                    reservationDTO.setId(reservation.getId());
                    reservationDTO.setStatus(ReservationStatus.SUCCESSFUL);
                    if (!updateReservation(reservationDTO)) {
                        return null;
                    }
                } else {
                    return null;
                }


            } else if (car.getStatus() != CarStatus.ACTIVE) {
                return null;
            }
            Renting renting = mapper.map(rentingDTO, Renting.class);
            car.setStatus(CarStatus.IN_RENT);
            renting.setUser(user);
            renting.setStartTime(new Date().getTime());
            renting.setStartLatLngX(car.getLatLngX());
            renting.setStartLatLngY(car.getLatLngY());
            car = carRepository.save(car);

            renting.setCar(car);
            renting = rentingRepository.save(renting);
            return getRentingDTO(renting);
        }

        return null;
    }


    @Override
    public boolean updateReservation(Reservation reservationDTO) {
        if (reservationDTO.getId() != null && reservationDTO.getStatus() != null) {
            Reservation reservation = reservationRepository.findById(reservationDTO.getId()).orElseThrow(NoSuchElementException::new);
            if (reservation.getStatus() == ReservationStatus.ACTIVE) {
                reservation.setStatus(reservationDTO.getStatus());
                reservation.setEndTime(new Date().getTime());
                // check car available
                Optional<Car> optionalCar = carRepository.findById(reservation.getCar().getId());
                Car car;
                if (optionalCar.isPresent()) {
                    car = optionalCar.get();
                } else {
                    return false;
                }
                if (car.getStatus() != CarStatus.RESERVED) {
                    return false;
                }
                // change car status
                if (reservationDTO.getStatus() == ReservationStatus.CANCELED) {
                    car.setStatus(CarStatus.ACTIVE);
                } else if (reservationDTO.getStatus() == ReservationStatus.SUCCESSFUL) {
                    car.setStatus(CarStatus.IN_RENT);
                }
                carRepository.save(car);
                reservationRepository.save(reservation);
                return true;
            }
        }
        return false;
    }

    @Override
    public RentingDTO endRenting(Long id, RentingDTO rentingDTO) {

        if (id != null) {
            User user = getLoggedUser();
            Renting renting = rentingRepository.findRentingById(id);
            if (renting != null) {
                if (rentingDTO.getCar().getId().equals(renting.getCar().getId())) {
                    Car car = carRepository.findCarById(rentingDTO.getCar().getId());
                    if (renting.getUser().getId().equals(user.getId())) {

                        renting.getCar().setStatus(CarStatus.ACTIVE);

                        renting.setEndTime(new Date().getTime());
                        renting.setEndLatLngX(car.getLatLngX());
                        renting.setEndLatLngY(car.getLatLngY());
                        renting.setTotalPrice(countTotalPrice(renting));
                        renting.setStars(rentingDTO.getStars());
                        renting.setComment(rentingDTO.getComment());
                        user.setAccountBalance(user.getAccountBalance() - renting.getTotalPrice());
                        MoneyTransfer newMoneyTransfer = new MoneyTransfer(renting.getTotalPrice(), user, PaymentStatus.LOCAL_TRANSFER);
                        userRepository.save(user);
                        moneyTransferRepository.save(newMoneyTransfer);
                        if (rentingDTO.getDistance() != null)
                            renting.setDistance(rentingDTO.getDistance());
                        carRepository.save(renting.getCar());

                        return getRentingDTO(rentingRepository.save(renting));
                    }
                }
            }
        }

        return null;
    }

    @Override
    public List<TariffPlanDTO> getTariffs() {
        List<TariffPlanDTO> tariffPlanDTOS = new ArrayList<>();
        for (TariffPlan t : tariffRepository.getSortedTariffs()) {
            tariffPlanDTOS.add(mapper.map(t, TariffPlanDTO.class));
        }
        return tariffPlanDTOS;
    }

    @Override
    public HistoryDTO getMyLastHistory(Integer page, Integer pageSize) {
        User user = getLoggedUser();
        Integer limit = (pageSize != null) ? pageSize : Constants.DEFAULT_LIMIT;
        Integer offset = (page != null) ? (page - 1) * limit : Constants.DEFAULT_OFFSET;
        HistoryDTO response = new HistoryDTO();
        response.setTotalResults(rentingRepository.getTotalNumberOfRentingsByUserId(user.getId()));
        List<Renting> rentings = Lists.newArrayList(rentingRepository.getMyLastHistory(user.getId(), offset, limit));
        List<RentingDTO> rentingDTOS = new ArrayList<>();
        for (Renting r : rentings) {
            rentingDTOS.add(getRentingDTO(r));
        }
        response.setHistoryList(rentingDTOS);
        return response;
    }

    private RentingDTO getRentingDTO(Renting renting) {
        if (renting != null) {
            RentingDTO dto = mapper.map(renting, RentingDTO.class);
            dto.setCar(mapper.map(renting.getCar(), CarDTO.class));
            if (renting.getTariff() != null)
                dto.setTariff(mapper.map(renting.getTariff(), TariffPlanDTO.class));
            else
                dto.setTariff(null);
            return dto;
        }
        return null;
    }

    private Double countTotalPrice(Renting renting) {
        long timeInRent = renting.getEndTime() - renting.getStartTime();
        List<TariffPlan> tarrifs = Lists.newArrayList(tariffRepository.getSortedTariffs());
        Long timeInMin = timeInRent / 1000 / 60;
        renting.setTotalPrice(0.);
        for (TariffPlan tariff : tarrifs) {
            renting.setTotalPrice(renting.getTotalPrice() + tariff.getPrice());
            renting.setTariff(tariff);
            if (timeInMin <= tariff.getMinutes()) {
                break;
            }
        }
        return renting.getTotalPrice();
    }

    private User getLoggedUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String phone = auth.getName(); // get logged user
        User user = userRepository.findByPhone(phone);
        return user;
    }
}
