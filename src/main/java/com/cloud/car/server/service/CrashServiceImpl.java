package com.cloud.car.server.service;

import com.cloud.car.server.model.Car;
import com.cloud.car.server.model.CarCrashAlerts;
import com.cloud.car.server.model.dto.carCrashAlertDTO.CarCrashAlertAdminResponseDTO;
import com.cloud.car.server.model.dto.carCrashAlertDTO.CarCrashAlertDTO;
import com.cloud.car.server.model.enums.CarStatus;
import com.cloud.car.server.repository.CarCrashAlertRepository;
import com.cloud.car.server.repository.CarRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Optional;

@Service
public class CrashServiceImpl {

    private final CarCrashAlertRepository crashAlertRepository;
    private final CarRepository carRepository;
    private final ModelMapper mapper;

    private final Type listType = new TypeToken<List<CarCrashAlertAdminResponseDTO>>() {}.getType();

    @Autowired
    public CrashServiceImpl(CarCrashAlertRepository crashAlertRepository,
                            CarRepository carRepository,
                            ModelMapper mapper) {
        this.crashAlertRepository = crashAlertRepository;
        this.carRepository = carRepository;
        this.mapper = mapper;
    }

    public List<CarCrashAlertAdminResponseDTO> getCrashes() {
        return mapper.map(crashAlertRepository.findAll(), listType);
    }

    public CarCrashAlertAdminResponseDTO updateCrash(Long id, CarCrashAlertDTO crashAlertDTO) {
        Optional<CarCrashAlerts> carCrashAlertOpt = crashAlertRepository.findById(id);

        if (carCrashAlertOpt.isPresent()) {
            CarCrashAlerts carCrashAlert = carCrashAlertOpt.get();
            carCrashAlert.setDescription(crashAlertDTO.getDescription());
            carCrashAlert.setCar(mapper.map(crashAlertDTO.getCar(), Car.class));
            carCrashAlert = crashAlertRepository.save(carCrashAlert);
            return mapper.map(carCrashAlert, CarCrashAlertAdminResponseDTO.class);
        }
        return null;
    }

    public void deleteCrash(Long id) {
        Optional<CarCrashAlerts> alert = crashAlertRepository.findById(id);
        if (alert.isPresent()) {
            Optional<Car> carOpt = carRepository.findById(alert.get().getCar().getId());
            if (carOpt.isPresent()){
                Car car = carOpt.get();
                if (!car.getStatus().equals(CarStatus.ACTIVE) && !car.getStatus().equals(CarStatus.DISACTIVE)) {
                    car.setStatus(CarStatus.ACTIVE);
                    carRepository.save(car);
                }
            }
        }
        crashAlertRepository.deleteById(id);
    }
}
