package com.cloud.car.server.service;

import com.cloud.car.server.model.TariffPlan;
import com.cloud.car.server.model.dto.TariffPlanDTO;
import com.cloud.car.server.model.enums.SimpleStatus;
import com.cloud.car.server.repository.TariffRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TariffServiceImpl implements TariffService {

    private TariffRepository repository;
    private ModelMapper mapper;

    @Autowired
    public TariffServiceImpl(TariffRepository repository,
                             ModelMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public TariffPlanDTO addTariff(TariffPlanDTO tariffPlanDTO) {
        TariffPlan tariffPlan = mapper.map(tariffPlanDTO, TariffPlan.class);
        tariffPlan.setStatus(SimpleStatus.ACTIVE);
        tariffPlan = repository.save(tariffPlan);
        return mapper.map(tariffPlan, TariffPlanDTO.class);
    }

    @Override
    public TariffPlanDTO updateTariff(Long id, TariffPlanDTO tariffPlanDTO) {
        Optional<TariffPlan> optionalTariffPlan = repository.findById(id);
        if (optionalTariffPlan.isPresent()) {
            TariffPlan dto = mapper.map(tariffPlanDTO, TariffPlan.class);
            TariffPlan tariffPlanDB = optionalTariffPlan.get();
            dto.setId(id);
            if (!tariffPlanDB.equals(dto)) {
                tariffPlanDB = repository.save(dto);
                return mapper.map(tariffPlanDB, TariffPlanDTO.class);
            }
        }
        return null;
    }

    @Override
    public void deleteTariff(Long id) {
        repository.deleteById(id);
    }
}
