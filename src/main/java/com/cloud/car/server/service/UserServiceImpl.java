package com.cloud.car.server.service;

import com.cloud.car.server.exception.UserExistsException;
import com.cloud.car.server.model.*;
import com.cloud.car.server.model.dto.AddressDTO;
import com.cloud.car.server.model.dto.userDTO.UserDetailsDTO;
import com.cloud.car.server.model.dto.userDTO.UserPasswordDTO;
import com.cloud.car.server.model.dto.userDTO.UserRegistrationDTO;
import com.cloud.car.server.repository.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.logging.Logger;

@Service
public class UserServiceImpl implements UserService {

    private Logger log = Logger.getLogger(this.getClass().getName());
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final AddressRepository addressRepository;
    private final CityRepository cityRepository;
    private final RegionRepository regionRepository;
    private final CountryRepository countryRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final ModelMapper mapper;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           RoleRepository roleRepository,
                           AddressRepository addressRepository,
                           CityRepository cityRepository,
                           RegionRepository regionRepository,
                           CountryRepository countryRepository,
                           BCryptPasswordEncoder bCryptPasswordEncoder,
                           ModelMapper mapper) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.addressRepository = addressRepository;
        this.cityRepository = cityRepository;
        this.regionRepository = regionRepository;
        this.countryRepository = countryRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.mapper = mapper;
    }

    @Override
    public List<UserDetailsDTO> getUsersDetails() {
        List<User> users = userRepository.findAll();
        List<UserDetailsDTO> dtos = new ArrayList<>();
        for (User u : users) {
            dtos.add(getUserDetailsDTO(u));
        }
        return dtos;
    }

    @Override
    public boolean registerUser(UserRegistrationDTO registrationDTO) throws UserExistsException {
        if (userExist(registrationDTO.getPhone()))
            throw new UserExistsException("User already exist");
        if (!isNewPasswordEqualToConfirmPassword(registrationDTO))
            return false;

        Set<Role> roles = new HashSet<>();
        roles.add(roleRepository.findByName("USER"));
        User user1 = User.builder()
                .email(registrationDTO.getEmail())
                .phone(registrationDTO.getPhone())
                .accountBalance(0.0)
                .password(bCryptPasswordEncoder.encode(registrationDTO.getNewPassword()))
                .name(registrationDTO.getName())
                .surname(registrationDTO.getSurname())
                .registrationDate(new Date())
                .roles(roles)
                .build();

        user1 = userRepository.save(user1);
        log.info("Successful registration " + user1.getPhone());
        return true;
    }

    @Override
    public UserDetailsDTO getUserDetails() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String phone = auth.getName();
        User user = userRepository.findByPhone(phone);

        return getUserDetailsDTO(user);
    }

    @Override
    public UserDetailsDTO updateUserDetails(UserDetailsDTO userDetailsDTO) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String phone = auth.getName();
        User user = userRepository.findByPhone(phone);

        if (user != null) {
            // update address
            if (userDetailsDTO.getAddress() != null) {
                City city = mapper.map(userDetailsDTO.getAddress().getCity(), City.class);
                Region region = mapper.map(userDetailsDTO.getAddress().getCity().getRegion(), Region.class);
                Country country = mapper.map(userDetailsDTO.getAddress().getCity().getRegion().getCountry(), Country.class);
                region.setCountry(country);
                city.setRegion(region);
                Address address1 = mapper.map(userDetailsDTO.getAddress(), Address.class);
                region.setCountry(countryRepository.save(country));
                city.setRegion(regionRepository.save(region));
                address1.setCity(cityRepository.save(city));
                address1 = addressRepository.save(address1);
                user.setAddress(address1);
            }

            if (userDetailsDTO.getCardNumber() != null) user.setCardNumber(userDetailsDTO.getCardNumber());
            if (userDetailsDTO.getEmail() != null) user.setEmail(userDetailsDTO.getEmail());
            if (userDetailsDTO.getPhone() != null) user.setPhone(userDetailsDTO.getPhone());
            if (userDetailsDTO.getName() != null) user.setName(userDetailsDTO.getName());
            if (userDetailsDTO.getSurname() != null) user.setSurname(userDetailsDTO.getSurname());
            if (userDetailsDTO.getPhoto() != null) user.setPhoto(userDetailsDTO.getPhoto());
            if (userDetailsDTO.getStatus() != null) user.setStatus(userDetailsDTO.getStatus());
            if (userDetailsDTO.getStars() != null) user.setStars(userDetailsDTO.getStars());

            user = userRepository.save(user);

            return getUserDetailsDTO(user);
        }
        return null;
    }

    @Override
    public boolean changePassword(UserPasswordDTO passwordDTO) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String phone = auth.getName();
        User user = userRepository.findByPhone(phone);
        user.setPassword(bCryptPasswordEncoder.encode(passwordDTO.getNewPassword()));
        userRepository.save(user);
        return true;
    }

    private boolean isNewPasswordEqualToConfirmPassword(UserRegistrationDTO registrationDTO) {
        return registrationDTO.getNewPassword() != null &&
                registrationDTO.getConfirmPassword() != null &&
                !registrationDTO.getNewPassword().isEmpty() &&
                registrationDTO.getNewPassword().equals(registrationDTO.getConfirmPassword());
    }


    private boolean userExist(String userDTO) {
        return userRepository.findByPhone(userDTO) != null;
    }

    private UserDetailsDTO getUserDetailsDTO(User user) {
        UserDetailsDTO dto = mapper.map(user, UserDetailsDTO.class);
        AddressDTO addressDTO = null;
        if (user.getAddress() != null) {
            addressDTO = mapper.map(user.getAddress(), AddressDTO.class);
        }
        dto.setAddress(addressDTO);
        return dto;
    }


}