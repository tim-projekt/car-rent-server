package com.cloud.car.server.service;

import com.cloud.car.server.exception.UserExistsException;
import com.cloud.car.server.model.auth.CustomUserDetails;
import com.cloud.car.server.model.dto.userDTO.UserDetailsDTO;
import com.cloud.car.server.model.dto.userDTO.UserPasswordDTO;
import com.cloud.car.server.model.dto.userDTO.UserRegistrationDTO;

import java.util.List;

public interface UserService {

    List<UserDetailsDTO> getUsersDetails();
    UserDetailsDTO getUserDetails();
    UserDetailsDTO updateUserDetails(UserDetailsDTO userDetailsDTO);
    boolean registerUser(UserRegistrationDTO registrationDTO) throws UserExistsException;

    boolean changePassword(UserPasswordDTO passwordDTO);
}
