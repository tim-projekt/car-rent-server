package com.cloud.car.server.service;

import com.cloud.car.server.model.dto.ReservationDTO;

public interface ReservationService {

    ReservationDTO createReservation(ReservationDTO reservation);
    ReservationDTO getReservations();
    ReservationDTO getReservation(Long id);
    boolean deleteReservation(Long id);
}
