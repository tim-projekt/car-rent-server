package com.cloud.car.server.service;

import com.cloud.car.server.model.dto.TariffPlanDTO;

public interface TariffService {
    TariffPlanDTO addTariff(TariffPlanDTO tariffPlan);
    TariffPlanDTO updateTariff(Long id, TariffPlanDTO tariffPlanDTO);
    void deleteTariff(Long id);
}
