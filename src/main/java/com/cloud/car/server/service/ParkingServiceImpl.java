package com.cloud.car.server.service;

import com.cloud.car.server.model.Parking;
import com.cloud.car.server.model.dto.parkingDTO.ParkingCreateDTO;
import com.cloud.car.server.model.dto.parkingDTO.ParkingDTO;
import com.cloud.car.server.model.enums.SimpleStatus;
import com.cloud.car.server.repository.ParkingRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class ParkingServiceImpl implements ParkingService {

    private final ParkingRepository parkingRepository;
    private final ModelMapper mapper;

    private Type listType = new TypeToken<List<ParkingDTO>>() {}.getType();

    public ParkingServiceImpl(ParkingRepository parkingRepository,
                              ModelMapper mapper) {
        this.parkingRepository = parkingRepository;
        this.mapper = mapper;
    }

    @Override
    public ParkingDTO getParking(Long id) {
        return mapper.map(parkingRepository.findParkingById(id), ParkingDTO.class);
    }

    @Override
    public List<ParkingDTO> getParkings() {
        return mapper.map(parkingRepository.findAll(), listType);
    }

    @Override
    public ParkingDTO createParking(ParkingCreateDTO parkingDTO) {
        Parking parking = mapper.map(parkingDTO, Parking.class);
        parking.setStatus(SimpleStatus.DISACTIVE);
        return mapper.map(parkingRepository.save(parking), ParkingDTO.class);
    }

    @Override
    public ParkingDTO updateParking(Long id, ParkingDTO createDTO) {
        Parking parking = parkingRepository.findParkingById(id);
        if (parking != null) {
            if (!createDTO.equals(mapper.map(parking, ParkingDTO.class))) {
                Parking parking1 = mapper.map(createDTO, Parking.class);
                parking1.setId(parking.getId());
                return mapper.map(parkingRepository.save(parking1), ParkingDTO.class);
            }
        }
        return null;
    }

    @Override
    public void deleteParking(Long id) {
        parkingRepository.deleteById(id);
    }
}
