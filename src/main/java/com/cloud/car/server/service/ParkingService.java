package com.cloud.car.server.service;

import com.cloud.car.server.model.dto.parkingDTO.ParkingCreateDTO;
import com.cloud.car.server.model.dto.parkingDTO.ParkingDTO;

import java.util.List;

public interface ParkingService {
    ParkingDTO getParking(Long id);
    List<ParkingDTO> getParkings();
    ParkingDTO createParking(ParkingCreateDTO parking);
    void deleteParking(Long id);
    ParkingDTO updateParking(Long id, ParkingDTO createDTO);
}
