package com.cloud.car.server.constant;

public class Constants {

    public static final String ROLE_USER = "ROLE_USER";
    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    public static final Integer DEFAULT_OFFSET = 0;
    public static final Integer DEFAULT_LIMIT = 10;
    public static final Long CANCEL_TIME = 900000L; // 900sec(15min)
}
