package com.cloud.car.server.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class ResourceNotAcceptable extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public ResourceNotAcceptable(String message) {
        super(message);
    }

    public ResourceNotAcceptable(String message, Throwable cause) {
        super(message, cause);
    }
}
