package com.cloud.car.server.exception;

public class AccountBalanceUnderLimitException extends Exception {
    public AccountBalanceUnderLimitException(String message) {
        super(message);
    }
}
