package com.cloud.car.server.model.auth;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class LoginUser {
    @NotNull(message = "username cannot be null")
    private String phone;

    @NotNull(message = "password cannot be null")
    private String password;
}
