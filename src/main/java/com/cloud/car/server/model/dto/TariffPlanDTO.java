package com.cloud.car.server.model.dto;

import com.cloud.car.server.model.enums.SimpleStatus;
import com.cloud.car.server.model.enums.TariffType;
import lombok.Data;

@Data
public class TariffPlanDTO {
    private Long id;
    private String name;
    private Integer minutes;
    private Double price;
    private SimpleStatus status;
    private TariffType type;
}
