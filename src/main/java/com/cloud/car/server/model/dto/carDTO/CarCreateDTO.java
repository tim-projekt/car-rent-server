package com.cloud.car.server.model.dto.carDTO;

import com.cloud.car.server.model.enums.CarStatus;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class CarCreateDTO {
    @NotNull
    private String model;
    @NotNull
    @Enumerated(EnumType.STRING)
    private CarStatus status;
    @NotNull
    private String registrationNumber;
    private Date purchaseDate;
    @NotNull
    private Double latLngX;
    @NotNull
    private Double latLngY;
}
