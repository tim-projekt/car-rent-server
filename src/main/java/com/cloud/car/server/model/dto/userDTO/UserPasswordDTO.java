package com.cloud.car.server.model.dto.userDTO;

import lombok.Data;

@Data
public class UserPasswordDTO {

    private String password;
    private String newPassword;
}
