package com.cloud.car.server.model;

import com.cloud.car.server.model.enums.SimpleStatus;
import com.cloud.car.server.model.enums.TariffType;
import lombok.Data;
import org.checkerframework.common.aliasing.qual.Unique;

import javax.persistence.*;

@Data
@Entity
@Table
public class TariffPlan {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Unique
	private String name;
	private Integer minutes;
	private Double price;
	@Enumerated(EnumType.STRING)
	private SimpleStatus status;
	@Enumerated(EnumType.STRING)
	private TariffType type;
}
