package com.cloud.car.server.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table
public class Renting {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@JsonIgnore
	@OneToOne
	@JoinColumn(name = "user_id")
	private User user;
	@OneToOne
	@JoinColumn(name = "car_id")
	private Car car;
	@OneToOne
	@JoinColumn(name = "tariff_plan_id")
	private TariffPlan tariff;
	private Double totalPrice;
	private Double startLatLngX;
	private Double startLatLngY;
	private Double endLatLngX;
	private Double endLatLngY;
	private Long startTime;
	private Long endTime;
	private Long distance;
	private Double stars;
	private String comment;
}
