package com.cloud.car.server.model.auth;

import lombok.Data;

import java.util.Date;

@Data
public class AuthToken {

    private String token;
    private Date expirationTime;
}
