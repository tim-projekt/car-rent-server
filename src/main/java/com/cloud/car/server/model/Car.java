package com.cloud.car.server.model;

import com.cloud.car.server.model.enums.CarStatus;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table
public class Car  {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String model;
	@Enumerated(EnumType.STRING)
	private CarStatus status;
	private String registrationNumber;
	private Date purchaseDate;
	private Double latLngX;
	private Double latLngY;


}
