package com.cloud.car.server.model.auth;

public class Constants {

    private static final long ACCESS_TOKEN_VALIDITY_SECONDS = 24*60*60;
    private static final long VALIDITY_NUMBER_OF_DAYS = 1;
    public static final long ACCESS_TOKEN_VALIDITY = ACCESS_TOKEN_VALIDITY_SECONDS * VALIDITY_NUMBER_OF_DAYS;
    public static final String SIGNING_KEY = "oerjovns34854gh49wv894w94ghs9KJHfFH43I-323432q2";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String AUTHORITIES_KEY = "scopes";
}
