package com.cloud.car.server.model.dto.carDTO;

import com.cloud.car.server.model.enums.CarStatus;
import lombok.Data;

import java.util.Date;

@Data
public class CarDTO {
    private Long id;
    private String model;
    private CarStatus status;
    private String registrationNumber;
    private Date purchaseDate;
    private Double latLngX;
    private Double latLngY;
}
