package com.cloud.car.server.model;

import com.cloud.car.server.model.enums.ReservationStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table
public class Reservation {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@JsonIgnore
	@OneToOne
	@JoinColumn(name = "user_id")
	private User user;
	@OneToOne
	@JoinColumn(name = "car_id")
	private Car car;
	@Enumerated(EnumType.STRING)
	private ReservationStatus status;
	private Long startTime;
	private Long endTime;
	


}
