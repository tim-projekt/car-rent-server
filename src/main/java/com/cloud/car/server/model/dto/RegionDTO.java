package com.cloud.car.server.model.dto;

import lombok.Data;

@Data
public class RegionDTO {

    private Long id;
    private String name;
    private CountryDTO country;
}
