package com.cloud.car.server.model.enums;

public enum ReservationStatus {
	ACTIVE, SUCCESSFUL, CANCELED
}
