package com.cloud.car.server.model.enums;

public enum CarStatus {
	IN_RENT, RESERVED, ACTIVE, DISACTIVE, BREAK, IN_SERVICE
}
