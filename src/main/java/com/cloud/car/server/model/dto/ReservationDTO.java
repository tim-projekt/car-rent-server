package com.cloud.car.server.model.dto;

import com.cloud.car.server.model.dto.carDTO.CarDTO;
import com.cloud.car.server.model.enums.ReservationStatus;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ReservationDTO {

    private Long id;
    @NotNull
    private CarDTO car;
    private ReservationStatus status;
    private Long startTime;
    private Long endTime;
}
