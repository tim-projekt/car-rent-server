package com.cloud.car.server.model.dto;

import lombok.Data;

@Data
public class CountryDTO {

    private Long id;
    private String name;

}
