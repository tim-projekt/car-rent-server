package com.cloud.car.server.model.dto;

import lombok.Data;

import java.util.List;

@Data
public class HistoryDTO {
	private Integer totalResults;
	private List<RentingDTO> historyList;

}
