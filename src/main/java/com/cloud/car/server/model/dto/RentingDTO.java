package com.cloud.car.server.model.dto;

import com.cloud.car.server.model.dto.carDTO.CarDTO;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class RentingDTO {

    private Long id;
    @NotNull
    private CarDTO car;
    private TariffPlanDTO tariff;
    private Double totalPrice;
    private Double startLatLngX;
    private Double startLatLngY;
    private Double endLatLngX;
    private Double endLatLngY;
    private Long startTime;
    private Long endTime;
    private Long distance;
    private Double stars;
    private String comment;
}
