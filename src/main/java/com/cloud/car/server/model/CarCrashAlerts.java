package com.cloud.car.server.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table
public class CarCrashAlerts {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;
	@ManyToOne
	@JoinColumn(name = "car_id")
	private Car car;
	private String description;
}
