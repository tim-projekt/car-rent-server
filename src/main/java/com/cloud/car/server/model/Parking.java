package com.cloud.car.server.model;

import com.cloud.car.server.model.enums.SimpleStatus;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table
public class Parking {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private Double latLngX;
	private Double latLngY;
	private String name;
	private Integer parkingSpaces;
	@Enumerated(EnumType.STRING)
	private SimpleStatus status;
	private Integer freeSpace;
}
