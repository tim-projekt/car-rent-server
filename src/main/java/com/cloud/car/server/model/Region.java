package com.cloud.car.server.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table
public class Region {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    @ManyToOne()
    @JoinColumn(name = "country_id", nullable = false)
    private Country country;


}
