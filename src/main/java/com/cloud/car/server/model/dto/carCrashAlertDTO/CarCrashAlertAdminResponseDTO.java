package com.cloud.car.server.model.dto.carCrashAlertDTO;

import com.cloud.car.server.model.dto.carDTO.CarDTO;
import com.cloud.car.server.model.dto.userDTO.UserCrashDetailsDTO;
import lombok.Data;

@Data
public class CarCrashAlertAdminResponseDTO {
    private Long id;
    private UserCrashDetailsDTO user;
    private CarDTO car;
    private String description;
}
