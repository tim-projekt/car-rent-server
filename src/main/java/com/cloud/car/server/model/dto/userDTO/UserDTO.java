package com.cloud.car.server.model.dto.userDTO;

import com.cloud.car.server.model.Address;
import com.cloud.car.server.model.Role;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.Set;

@Data
public class UserDTO {

	private Address address;
	private Long cardNumber;
	@NotNull(message = "email cannot be null")
	private String email;
	@NotNull(message = "phone cannot be null")
	private String phone;
	private String password;
	private String newPassword;
	private String confirmPassword;
	@NotNull(message = "name cannot be null")
	private String name;
	@NotNull(message = "surname cannot be null")
	private String surname;
	private String photo;
	private Double accountBalance;
	private String status;
	private Integer stars;
	private Set<Role> roles;

}
