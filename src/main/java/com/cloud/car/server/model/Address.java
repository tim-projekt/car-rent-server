package com.cloud.car.server.model;

import lombok.Data;

import javax.persistence.*;


@Data
@Entity
@Table(name = "addresses")
public class Address {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String street;
	private String postal;
	@ManyToOne
	@JoinColumn(name = "city_id", nullable = false)
	private City city;
}
