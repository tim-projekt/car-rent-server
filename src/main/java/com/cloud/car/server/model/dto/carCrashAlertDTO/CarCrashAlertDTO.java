package com.cloud.car.server.model.dto.carCrashAlertDTO;

import com.cloud.car.server.model.dto.carDTO.CarDTO;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CarCrashAlertDTO {
    @NotNull
    private CarDTO car;
    @NotNull
    private String description;
}
