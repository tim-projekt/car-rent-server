package com.cloud.car.server.model.dto.carCrashAlertDTO;

import com.cloud.car.server.model.dto.carDTO.CarDTO;
import lombok.Data;

@Data
public class CarCrashAlertResponseDTO {
    private Long id;
    private CarDTO car;
    private String description;
}
