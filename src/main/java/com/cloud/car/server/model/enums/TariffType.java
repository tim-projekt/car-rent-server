package com.cloud.car.server.model.enums;

public enum TariffType {
	BASE, SPECIAL
}
