package com.cloud.car.server.model.enums;

public enum PaymentStatus {
	CREATED, CANCELED, DENIED, RESERVED, PENDING, COMPLETED, LOCAL_TRANSFER
}
