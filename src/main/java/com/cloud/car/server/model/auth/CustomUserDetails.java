package com.cloud.car.server.model.auth;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Set;

public class CustomUserDetails extends User implements UserDetails {

	private static final long serialVersionUID = 1L;
	
	public Long userId;
    public String phone;

    public CustomUserDetails(String username, String password, Set<SimpleGrantedAuthority> authority, Long id, String phone) {
        super( username,  password,  authority);
        this.userId = id;
        this.phone = phone;
    }


    public Long getUserId()
    {
        return userId;
    }
    public Long getUserPhone()
    {
        return userId;
    }


    @Override
    public void eraseCredentials() {
        super.eraseCredentials();
        this.userId = null;
    }
}
