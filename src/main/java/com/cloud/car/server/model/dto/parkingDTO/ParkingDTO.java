package com.cloud.car.server.model.dto.parkingDTO;

import com.cloud.car.server.model.enums.SimpleStatus;
import lombok.Data;

@Data
public class ParkingDTO {

    private Long id;
    private Double latLngX;
    private Double latLngY;
    private String name;
    private Integer parkingSpaces;
    private SimpleStatus status;
    private Integer freeSpace;
}
