package com.cloud.car.server.model;

import com.cloud.car.server.model.enums.PaymentStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table
public class MoneyTransfer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Double amount;
    private Long time;
    @Enumerated(EnumType.STRING)
    private PaymentStatus status;
    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    public MoneyTransfer() {
    }

    // For payments
    public MoneyTransfer(Double amount, User user) {
        super();
        this.amount = amount;
        this.time = new Date().getTime();
        this.status = PaymentStatus.CREATED;
        this.user = user;
    }

    // For local payment of rentings
    public MoneyTransfer(Double amount, User user, PaymentStatus status) {
        super();
        this.amount = amount;
        this.time = new Date().getTime();
        this.status = status;
        this.user = user;
    }

}
