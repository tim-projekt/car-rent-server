package com.cloud.car.server.model.enums;

public enum SimpleStatus {
	ACTIVE, DISACTIVE, ARCHIVED
}
