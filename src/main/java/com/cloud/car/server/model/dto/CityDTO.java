package com.cloud.car.server.model.dto;

import lombok.Data;


@Data
public class CityDTO {

    private Long id;
    private String name;
    private RegionDTO region;
}
