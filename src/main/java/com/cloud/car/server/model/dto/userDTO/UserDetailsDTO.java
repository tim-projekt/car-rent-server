package com.cloud.car.server.model.dto.userDTO;

import com.cloud.car.server.model.dto.AddressDTO;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UserDetailsDTO {

    private AddressDTO address;
    private Long cardNumber;
    @NotNull(message = "email cannot be null")
    private String email;
    @NotNull(message = "phone cannot be null")
    private String phone;
    @NotNull(message = "name cannot be null")
    private String name;
    @NotNull(message = "surname cannot be null")
    private String surname;
    private String photo;
    private Double accountBalance;
    private String status;
    private Integer stars;
}
