package com.cloud.car.server.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Set;

@Data
@Builder
@Entity
@Table(name = "users")
public class User {
	@Id
	@JsonIgnore
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@ManyToOne
	@JoinColumn(name = "address_id")
	private Address address;
	private Long cardNumber;
	private String email;
	@NotNull
	private String phone;
	@JsonIgnore
	private String password;
	private String name;
	private String surname;
	private String photo;
	private Date registrationDate;
	private Double accountBalance;
	private String status;
	private Integer stars;
	@JsonIgnore
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "USER_ROLES", joinColumns = {
			@JoinColumn(name = "USER_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "ROLE_ID") })
	private Set<Role> roles;

	@Tolerate
    public User() {
    }
}