package com.cloud.car.server.model.dto;

import lombok.Data;

@Data
public class AddressDTO {

    private Long id;
    private String street;
    private String postal;
    private CityDTO city;
}
