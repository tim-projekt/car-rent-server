package com.cloud.car.server.model.dto.userDTO;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Builder
@Data
public class UserRegistrationDTO {

    @NotNull(message = "email cannot be null")
    @Email
    private String email;
    @NotNull(message = "phone cannot be null")
    private String phone;
    private String password;
    private String newPassword;
    private String confirmPassword;
    @NotNull(message = "name cannot be null")
    private String name;
    @NotNull(message = "surname cannot be null")
    private String surname;
}
