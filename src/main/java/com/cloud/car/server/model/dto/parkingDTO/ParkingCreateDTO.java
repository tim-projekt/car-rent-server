package com.cloud.car.server.model.dto.parkingDTO;

import com.cloud.car.server.model.enums.SimpleStatus;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ParkingCreateDTO {

    @NotNull
    private Double latLngX;
    @NotNull
    private Double latLngY;
    private String name;
    private Integer parkingSpaces;
    private SimpleStatus status;
    private Integer freeSpace;
}
