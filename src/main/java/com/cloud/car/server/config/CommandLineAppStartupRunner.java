package com.cloud.car.server.config;

import com.cloud.car.server.model.Role;
import com.cloud.car.server.model.User;
import com.cloud.car.server.repository.RoleRepository;
import com.cloud.car.server.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

@Component
public class CommandLineAppStartupRunner implements CommandLineRunner {

	private Logger log = Logger.getLogger(this.getClass().getName());

	private final UserRepository userRepository;
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	private final RoleRepository roleRepository;

	@Autowired
	public CommandLineAppStartupRunner(UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder, RoleRepository roleRepository) {
		this.userRepository = userRepository;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
		this.roleRepository = roleRepository;
	}

	//set up startup admin
	@Override
	public void run(String... args) {
		if(userRepository.findAll().iterator().hasNext())
			log.info("DB was already initialized with admin data");
		else {
			User user = User.builder()
					.email("admin@admin.com")
					.phone("+48000000000")
					.password(bCryptPasswordEncoder.encode("admin"))
					.name("Admin")
					.surname("Main")
					.accountBalance(0.0)
					.registrationDate(new Date())
					.build();
			Set<Role> roles = new HashSet<>();
			Role adminRole = new Role("ADMIN");
			Role userRole = new Role("USER");
			roles.add(adminRole);
			roles.add(userRole);
			user.setRoles(roles);
			userRepository.save(user);
			log.info("Successful initialization of new DB with admin data");
		}
	}
}
