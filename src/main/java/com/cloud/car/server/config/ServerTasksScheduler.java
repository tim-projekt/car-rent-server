package com.cloud.car.server.config;

import com.cloud.car.server.model.Reservation;
import com.cloud.car.server.model.enums.ReservationStatus;
import com.cloud.car.server.repository.ReservationRepository;
import com.cloud.car.server.service.RentingService;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;

import static com.cloud.car.server.constant.Constants.CANCEL_TIME;

@Component
public class ServerTasksScheduler {

	private final RentingService rentingService;
	private final ReservationRepository reservationRepository;

    @Autowired
    public ServerTasksScheduler(RentingService rentingService, ReservationRepository reservationRepository) {
        this.rentingService = rentingService;
        this.reservationRepository = reservationRepository;
    }

    @Scheduled(fixedDelay = 60000)
    public void fixedDelaySchedule() {
        System.out.println("get ACTIVE reservations every 60 seconds " + new Date());
        Iterable<Reservation> list = reservationRepository.getActiveReservations();
        if(list != null) {
        	  ArrayList<Reservation> reservesList = Lists.newArrayList(list);
              for(Reservation item : reservesList) {
              	System.out.println("Reservation: "+item.getId()
              						+" status: "+item.getStatus()
              						+" lost "+(new Date().getTime() - item.getStartTime())/1000/60 + " min");
              	if(new Date().getTime() - item.getStartTime() > CANCEL_TIME) {
              		System.out.println("CANCELED "+item.getId());
              		item.setStatus(ReservationStatus.CANCELED);
              		rentingService.updateReservation(item);
              	}
              }
        }
    }

    //every 5 minutes (seconds, minutes, hours, day of month, month, day of week, year(optional))
    @Scheduled(cron = "0 0/5 * * * ?")
    public void cronSchedule() {
        System.out.println("test alive server every 5 min" + new Date());
    }

}
