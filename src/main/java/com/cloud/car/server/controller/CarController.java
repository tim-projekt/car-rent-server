package com.cloud.car.server.controller;

import com.cloud.car.server.model.dto.carCrashAlertDTO.CarCrashAlertDTO;
import com.cloud.car.server.model.dto.carCrashAlertDTO.CarCrashAlertResponseDTO;
import com.cloud.car.server.model.dto.carDTO.CarDTO;
import com.cloud.car.server.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/cars")
public class CarController {

    private CarService carService;

    @Autowired
    public CarController(CarService carService) {
        this.carService = carService;
    }

    @GetMapping()
    public ResponseEntity<List<CarDTO>> getCars() {
        return new ResponseEntity<>(carService.getActiveCars(), HttpStatus.OK);
    }


    @GetMapping("/{id}")
    public ResponseEntity<CarDTO> getCar(@PathVariable Long id) {
        CarDTO car = carService.getCar(id);
        if (car != null)
            return new ResponseEntity<>(car, HttpStatus.OK);
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @PostMapping("/{id}/crash")
    public ResponseEntity<CarCrashAlertResponseDTO> addCarCrashAlert(@PathVariable Long id,
                                                                     @Valid @RequestBody CarCrashAlertDTO alert) {
        CarCrashAlertResponseDTO dto = carService.addCrashAlert(id, alert);
        if (dto != null) {
            return new ResponseEntity<>(dto, HttpStatus.CREATED);
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }


}
