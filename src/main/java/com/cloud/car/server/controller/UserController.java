package com.cloud.car.server.controller;

import com.cloud.car.server.exception.PaymentException;
import com.cloud.car.server.model.MoneyTransfer;
import com.cloud.car.server.model.User;
import com.cloud.car.server.model.dto.userDTO.UserDetailsDTO;
import com.cloud.car.server.model.dto.userDTO.UserPasswordDTO;
import com.cloud.car.server.repository.UserRepository;
import com.cloud.car.server.service.PaymentService;
import com.cloud.car.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.logging.Logger;

@RestController
@RequestMapping("/")
public class UserController {

    private final Logger log = Logger.getLogger(this.getClass().getName());
    private final UserRepository userRepository;
    private final UserService userService;
    private final PaymentService paymentService;

    @Autowired
    public UserController(UserRepository userRepository,
                          UserService userService,
                          PaymentService payuService) {
        this.userRepository = userRepository;
        this.userService = userService;
        this.paymentService = payuService;
    }

    @GetMapping("users")
    public ResponseEntity<List<UserDetailsDTO>> getAllUsers() {
        log.info("getAllUsers()");
        List<UserDetailsDTO> userDetailsDTOS = userService.getUsersDetails();
        return new ResponseEntity<>(userDetailsDTOS, HttpStatus.OK);
    }

    @PostMapping("users/changePassword")
    public ResponseEntity<?> changeUserPassword(@Valid @RequestBody UserPasswordDTO passwordDTO) {
        if (userService.changePassword(passwordDTO)) {
            return ResponseEntity.status(HttpStatus.OK).build();
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }


    @GetMapping("profile")
    public ResponseEntity<UserDetailsDTO> getProfile() {
        log.info("getProfile()");

        UserDetailsDTO dto = userService.getUserDetails();
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PutMapping("profile")
    public ResponseEntity<?> updateProfile(@RequestBody @Valid UserDetailsDTO userDTO) {
        log.info("updateProfile()");
        UserDetailsDTO dto = userService.updateUserDetails(userDTO);
        if (dto != null) {
            return new ResponseEntity<>(dto, HttpStatus.OK);
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }


    @GetMapping("profile/top-up")
    public ResponseEntity<?> topUp(@Param("amount") Double amount) throws PaymentException {
        log.info("topUp(" + amount + ")");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String phone = auth.getName(); // get logged user
        User user = userRepository.findByPhone(phone);

        return paymentService.initPayment(new MoneyTransfer(amount, user));
    }
}
