package com.cloud.car.server.controller;

import com.cloud.car.server.exception.AccountBalanceUnderLimitException;
import com.cloud.car.server.model.dto.HistoryDTO;
import com.cloud.car.server.model.dto.RentingDTO;
import com.cloud.car.server.model.dto.TariffPlanDTO;
import com.cloud.car.server.service.RentingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.logging.Logger;

@RestController
@RequestMapping("/rentings")
public class RentingController {

    private Logger log = Logger.getLogger(this.getClass().getName());
    private RentingService rentingService;

    @Autowired
    public RentingController(RentingService rentingService) {
        this.rentingService = rentingService;
    }

    @GetMapping()
    public ResponseEntity<RentingDTO> getMyRenting() {
        RentingDTO rentingDTO = rentingService.getUserRenting();
        if (rentingDTO != null) {
            return new ResponseEntity<>(rentingDTO, HttpStatus.OK);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<RentingDTO> getRentingById(@PathVariable Long id) {
        RentingDTO rentingDTO = rentingService.getRenting(id);
        if (rentingDTO != null) {
            return new ResponseEntity<>(rentingDTO, HttpStatus.OK);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @PostMapping()
    public ResponseEntity<?> startRentingCar(@Valid @RequestBody RentingDTO renting) throws AccountBalanceUnderLimitException {
        RentingDTO rentingDTO = rentingService.startRenting(renting);
        if (rentingDTO != null) {
            return new ResponseEntity<>(rentingDTO, HttpStatus.OK);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> endRenting(@PathVariable Long id,
                                        @Valid @RequestBody RentingDTO renting) {
        RentingDTO rentingDTO = rentingService.endRenting(id, renting);
        if (rentingDTO != null) {
            return new ResponseEntity<>(rentingDTO, HttpStatus.OK);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @GetMapping(value = "/lasthistory")
    public ResponseEntity<HistoryDTO> getMyLastHistory(@Param("page") Integer page, @Param("pageSize") Integer pageSize) {
        log.info("getMyLastHistory(page=" + page + ",pageSize=" + pageSize + ")");
        HistoryDTO dto = rentingService.getMyLastHistory(page, pageSize);
        if (dto != null) {
            return new ResponseEntity<>(dto, HttpStatus.OK);
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    @GetMapping(value = "/tariffs")
    public ResponseEntity<List<TariffPlanDTO>> getSortedTariffs() {
        List<TariffPlanDTO> dtos = rentingService.getTariffs();
        if (dtos != null) {
            return new ResponseEntity<>(dtos, HttpStatus.OK);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }
}
