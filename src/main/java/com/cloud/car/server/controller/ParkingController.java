package com.cloud.car.server.controller;

import com.cloud.car.server.model.dto.parkingDTO.ParkingCreateDTO;
import com.cloud.car.server.model.dto.parkingDTO.ParkingDTO;
import com.cloud.car.server.service.ParkingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/parkings")
public class ParkingController {

    private ParkingService parkingService;

    @Autowired
    public ParkingController(ParkingService parkingService) {
        this.parkingService = parkingService;
    }

    @GetMapping()
    public ResponseEntity<List<ParkingDTO>> getParkings() {
        return new ResponseEntity<>(parkingService.getParkings(), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<ParkingDTO> createParking(@Valid @RequestBody ParkingCreateDTO createDTO) {
        return new ResponseEntity<>(parkingService.createParking(createDTO), HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ParkingDTO> getParking(@PathVariable Long id) {
        ParkingDTO parking = parkingService.getParking(id);
        if (parking != null)
            return new ResponseEntity<>(parking, HttpStatus.OK);
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }


}
