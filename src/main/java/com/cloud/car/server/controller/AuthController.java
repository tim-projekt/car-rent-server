package com.cloud.car.server.controller;

import com.cloud.car.server.config.TokenProvider;
import com.cloud.car.server.exception.UserExistsException;
import com.cloud.car.server.model.auth.AuthToken;
import com.cloud.car.server.model.auth.LoginUser;
import com.cloud.car.server.model.dto.userDTO.UserRegistrationDTO;
import com.cloud.car.server.service.UserService;
import com.cloud.car.server.service.UserServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.logging.Logger;

@RestController
@RequestMapping("/auth")
public class AuthController {

	private final Logger log = Logger.getLogger(this.getClass().getName());
	private final TokenProvider jwtTokenUtil;
	private final AuthenticationManager authenticationManager;
	private final UserService userService;

	@Autowired
	public AuthController(TokenProvider jwtTokenUtil,
						  AuthenticationManager authenticationManager,
						  UserService userService) {
		this.jwtTokenUtil = jwtTokenUtil;
		this.authenticationManager = authenticationManager;
		this.userService = userService;
	}


	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseEntity<?> generateToken(@Valid @RequestBody LoginUser loginUser) throws AuthenticationException {
		log.info("generateToken()");
		try {
			final Authentication authentication = authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(
							loginUser.getPhone(),
							loginUser.getPassword()
					)
			);
			SecurityContextHolder.getContext().setAuthentication(authentication);
			final AuthToken token = jwtTokenUtil.generateToken(authentication);
			return ResponseEntity.ok(token);
		}
		catch (AuthenticationException e)
		{
			log.info("Unauthorized attempt to generate token");
			throw  e;
		}

	}
	
	@PostMapping(value = "/register")
	public ResponseEntity<?> register(@Valid @RequestBody UserRegistrationDTO userDTO) throws JsonProcessingException {
		try {
			if (userService.registerUser(userDTO)) {
				return new ResponseEntity<>(new ObjectMapper().writeValueAsString("User registered successfully"), HttpStatus.CREATED);
			} else {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
			}
		} catch (UserExistsException e) {
			return new ResponseEntity<>(new ObjectMapper().writeValueAsString(e.getMessage()), HttpStatus.CONFLICT);
		}
	}




}
