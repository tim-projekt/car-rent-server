package com.cloud.car.server.controller;

import com.cloud.car.server.model.dto.TariffPlanDTO;
import com.cloud.car.server.model.dto.carCrashAlertDTO.CarCrashAlertAdminResponseDTO;
import com.cloud.car.server.model.dto.carCrashAlertDTO.CarCrashAlertDTO;
import com.cloud.car.server.model.dto.carCrashAlertDTO.CarCrashAlertResponseDTO;
import com.cloud.car.server.model.dto.carDTO.CarCreateDTO;
import com.cloud.car.server.model.dto.carDTO.CarDTO;
import com.cloud.car.server.model.dto.parkingDTO.ParkingDTO;
import com.cloud.car.server.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.logging.Logger;

@RestController
@RequestMapping("/admin")
public class AdminController {

    private final Logger log = Logger.getLogger(this.getClass().getName());
    private final ParkingService parkingService;
    private final TariffService tariffService;
    private final CarService carService;
    private final CrashServiceImpl crashService;

    @Autowired
    public AdminController(ParkingService parkingService,
                           TariffService tariffService,
                           CarService carService,
                           CrashServiceImpl crashService) {
        this.parkingService = parkingService;
        this.tariffService = tariffService;
        this.carService = carService;
        this.crashService = crashService;
    }

    @GetMapping()
    public ResponseEntity<?> getAdmin() {
        log.info("getAdmin()");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String phone = auth.getName(); // get logged user

        return new ResponseEntity<>("Welcome " + phone + " to admin panel", HttpStatus.OK);
    }

    @PutMapping("/parkings/{id}")
    public ResponseEntity<ParkingDTO> updateParking(@PathVariable Long id,
                                                    @Valid @RequestBody ParkingDTO createDTO) {
        return new ResponseEntity<>(parkingService.updateParking(id, createDTO), HttpStatus.OK);
    }

    @DeleteMapping("/parkings/{id}")
    public ResponseEntity<?> deleteParking(@PathVariable Long id) {
        parkingService.deleteParking(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PostMapping("/tariffs")
    public ResponseEntity<TariffPlanDTO> addTariff(@Valid @RequestBody TariffPlanDTO tariffPlan) {
        TariffPlanDTO dto = tariffService.addTariff(tariffPlan);
        if (dto != null) {
            return new ResponseEntity<>(dto, HttpStatus.OK);
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    @PutMapping("/tariffs/{id}")
    public ResponseEntity<TariffPlanDTO> updateTariff(@PathVariable Long id, @Valid @RequestBody TariffPlanDTO tariffPlan) {
        TariffPlanDTO dto = tariffService.updateTariff(id, tariffPlan);
        if (dto != null) {
            return new ResponseEntity<>(dto, HttpStatus.OK);
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    @GetMapping("/cars")
    public ResponseEntity<List<CarDTO>> getAllCars() {
        return new ResponseEntity<>(carService.getAllCars(), HttpStatus.OK);
    }

    @PostMapping("/cars")
    public ResponseEntity<CarDTO> createCar(@Valid @RequestBody CarCreateDTO carDTO) {
        return new ResponseEntity<>(carService.createCar(carDTO), HttpStatus.CREATED);
    }

    @PutMapping("/cars/{id}")
    public ResponseEntity<CarDTO> updateCar(@PathVariable Long id, @Valid @RequestBody CarDTO carDTO) {
        CarDTO updatedCar = carService.updateCar(id, carDTO);
        if (updatedCar != null)
            return new ResponseEntity<>(updatedCar, HttpStatus.OK);
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @DeleteMapping("/cars/{id}")
    public ResponseEntity<?> deleteCar(@PathVariable Long id) {
        carService.deleteCar(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @GetMapping("/crashes")
    public ResponseEntity<?> getCrashes() {
        return new ResponseEntity<>(crashService.getCrashes(), HttpStatus.OK);
    }

    @PutMapping("/crashes/{id}")
    public ResponseEntity<CarCrashAlertAdminResponseDTO> updateCarCrash(@PathVariable Long id,
                                                                        @Valid @RequestBody CarCrashAlertDTO carCrashDTO) {
        CarCrashAlertAdminResponseDTO dto = crashService.updateCrash(id, carCrashDTO);
        if (dto != null) {
            return new ResponseEntity<>(dto, HttpStatus.OK);
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    @DeleteMapping("/crashes/{id}")
    public ResponseEntity<?> deleteCarCrash(@PathVariable Long id) {
        crashService.deleteCrash(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
