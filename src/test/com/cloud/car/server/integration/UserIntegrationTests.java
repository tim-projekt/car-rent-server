package com.cloud.car.server.integration;

import com.cloud.car.server.model.dto.userDTO.UserDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHeaders;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserIntegrationTests {

    @Autowired
    private MockMvc mockMvc;

    private static final ObjectMapper om = new ObjectMapper();


    @Test
    @Transactional
    public void registrationWithInvalidCredentialsShouldReturnClientError4xx() throws Exception
    {
        UserDTO newUser = new UserDTO();
        newUser.setNewPassword("qwerty");
        newUser.setConfirmPassword("fdsf");
        newUser.setPhone("+48666666666");


        mockMvc.perform(post("/auth/register")
                .content(om.writeValueAsString(newUser))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());

    }


    @Test
    @Transactional
    public void registrationWithEmptyCredentialsShouldReturn_406() throws Exception
    {
        UserDTO newUser = new UserDTO();
        mockMvc.perform(post("/auth/register")
                .content(om.writeValueAsString(newUser))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());

    }


    @Test
    @Transactional
    public void withoutUserRoleShouldNotReturnProfile() throws Exception
    {
        mockMvc.perform(get("/profile"))
                .andExpect(status().isUnauthorized());

    }


    @Test
    @Transactional
    @WithMockUser("ADMIN")
    public void withAdminRoleShouldReturnAllUsersInformation() throws Exception
    {
        mockMvc.perform(get("/users"))
                .andExpect(status().isOk());

    }

    @Test
    @Transactional
    public void withoutAdminRoleShouldNotReturnAllUsersInformation() throws Exception
    {
        mockMvc.perform(get("/users"))
                .andExpect(status().isUnauthorized());

    }




}
