package com.cloud.car.server.unit;


import com.cloud.car.server.model.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

import static com.cloud.car.server.model.auth.Constants.ACCESS_TOKEN_VALIDITY;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserUnitTests {


    @Test
    public void AccessTokenValidationShouldGreaterThen0() {
        Assert.assertTrue(ACCESS_TOKEN_VALIDITY > 0);
    }


}
